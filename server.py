from tornado import web, ioloop

from app import Index, EditTracks, AddTrack, DeleteTrack, SearchTrack

if __name__ == '__main__':
    app = web.Application([
        ('/', Index),
        (r'/edit_track/(\d+)/', EditTracks),
        (r'/add_track/', AddTrack),
        (r'/delete_track/', DeleteTrack),
        (r'/search_track/', SearchTrack)
    ],
        debug=True
    )
    app.listen(8888)
    ioloop.IOLoop.current().start()
