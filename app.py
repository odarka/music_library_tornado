from re import match

from sqlalchemy import or_
from sqlalchemy.orm import sessionmaker
from tornado import web

from db import engine
from models import Tracks


def check_duration(duration):
    if not match(r'^[1-9]\d*\:[0-5][0-9]$', duration):
        return "enter duration like 00:00"
    else:
        return ''


class Index(web.RequestHandler):

    def get(self):
        session = sessionmaker(bind=engine)()
        tracks = session.query(Tracks).all()

        self.render('templates/index.html', tracks=tracks)


class EditTracks(web.RequestHandler):

    def get(self, track_id):
        session = sessionmaker(bind=engine)()
        track = session.query(Tracks).get(track_id)
        self.render(
            'templates/edit_track.html',
            track=track,
            duration_error=''
        )

    def post(self, track_id):
        session = sessionmaker(bind=engine)()
        duration_error = check_duration(self.get_body_argument('duration'))
        track = session.query(Tracks).get(track_id)
        if duration_error:
            self.render(
                'templates/edit_track.html',
                track=track,
                duration_error=duration_error
            )
        else:
            session.query(Tracks).filter_by(id=track_id).update({'performer': self.get_body_argument('performer'),
                                                                 'track': self.get_body_argument('track'),
                                                                 'duration': self.get_body_argument('duration')})
            session.commit()
            self.redirect('/')


class AddTrack(web.RequestHandler):

    def get(self):
        self.render(
            'templates/add_track.html',
            duration_error='',
            performer='',
            track='',
        )

    def post(self):
        session = sessionmaker(bind=engine)()
        performer = self.get_body_argument('performer')
        track = self.get_body_argument('track')
        duration = self.get_body_argument('duration')
        duration_error = check_duration(duration)
        if duration_error:
            self.render(
                'templates/add_track.html',
                duration_error=duration_error,
                performer=performer,
                track=track
            )
        else:
            session.add(
                Tracks(performer=performer, track=track, duration=duration)
            )
            session.commit()
            self.redirect('/')


class DeleteTrack(web.RequestHandler):

    def post(self):
        session = sessionmaker(bind=engine)()
        id = self.get_body_argument('id_track')
        track = session.query(Tracks).get(id)
        session.delete(track)
        session.commit()
        self.redirect('/')


class SearchTrack(web.RequestHandler):

    def get(self):
        session = sessionmaker(bind=engine)()
        search_query = self.get_argument('search', '')

        if search_query == '':
            self.render(
                'templates/search_track.html',
                tracks=[],
                search_query=''
            )
        else:
            tracks_query = session.query(Tracks)
            search_query_lst = search_query.split(' ')
            likes = []
            for word in search_query_lst:
                word_formatted = "%{}%".format(word)
                likes.append(Tracks.performer.like(word_formatted))
                likes.append(Tracks.track.like(word_formatted)),
                likes.append(Tracks.duration.like(word_formatted))

            tracks_query = tracks_query.filter(or_(*likes))

            self.render(
                'templates/search_track.html',
                tracks=tracks_query.all(),
                search_query=search_query
            )
