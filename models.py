from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Tracks(Base):
    __tablename__ = 'tracks'
    id = Column(Integer, primary_key=True, autoincrement=True)
    performer = Column(String, nullable=False)
    track = Column(String, nullable=False)
    duration = Column(String, nullable=False)
